# def to_dict(lst):
#     new_dict = {}
#     for item in lst:
#         new_dict.update({item:item})
#     return new_dict
# print(to_dict(['1', '2', '3']))


# my_dict = {'first one': 'we can do it'}
# def biggest_dict(**kwargs):
#     my_dict.update(**kwargs)
# biggest_dict(a='b',c='1',d='e')
# biggest_dict(first=23,second=34,third=57)
# print(my_dict)


# from collections import Counter
# def count_it(sequence):
#     return dict(Counter([int(x) for x in sequence]).most_common(3))
# print(count_it('569484894855555444499999111'))


# from collections import OrderedDict
# x = OrderedDict({1:'1', 2:'2', 3:'3', 4:'4', 5:'5'})
# first = list(x.items())[0]
# last = list(x.items())[-1]
# x.move_to_end(key=first[0])
# x.move_to_end(key=last[0], last=False)
# second = list(x.items())[1]
# del x[second[0]]
# x['new_key']='new_value'
# print(x)
