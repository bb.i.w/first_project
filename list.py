# lst = ['а','к','о','р','т','с']
# print(lst[::-1])


# def change(lst):
#     lst[0], lst[-1] = lst[-1], lst[0]
#     return lst
# print(change([5,2,3,4,1]))

# def change(lst):
#     first=lst.pop()
#     last=lst.pop(0)
#     lst.append(last)
#     lst.insert(0, first)
#     return lst
# print(change([5,2,3,4,1]))


# def to_list(*args):
#     return list(args)
# print(to_list(1,2,3))
# print(to_list(1,2,3,4,5))
# print(to_list(1,2,3,4,5,6,7,8))


# def useless(s):
#     return max(s)/len(s)
# print(useless([1,2,3,56,41,22]))


# def list_sort(lst):
#     lst.sort(key=lambda i: abs(i), reverse=True)
#     return lst
# print(list_sort([11,56,23,42]))


# def all_eq(lst):
#     max_item = max(lst, key=lambda x: len(x))
#     max_len = len(max_item)
#     return [item.ljust(max_len, '_') for item in lst]
# print(all_eq(['5656', '698', '-68', '73', '1', '-13', '0']))
# print(all_eq(['699', '698', '47', '1']))
# print(all_eq(['56', '4', '68', '1', '2']))
