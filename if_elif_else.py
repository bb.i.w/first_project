# def is_alive(health):
#     if health<=0:
#         return False
#     else:
#         return True
# print(is_alive(-56))
# print(is_alive(0))
# print(is_alive(57))


# months = {
#     1: 'январе',
#     2: 'феврале',
#     3: 'марте',
#     4: 'апреле',
#     5: 'мае',
#     6: 'июне',
#     7: 'июле',
#     8: 'августе',
#     9: 'сентябре',
#     10: 'октябре',
#     11: 'ноябре',
#     12: 'декабре'
# }
# def season_events(number_of_month):
#     if not isinstance(number_of_month, int) or not 1<=number_of_month<=12:
#         print('Требуется ввести реальный номер месяца')
#     elif number_of_month in range(3, 6):
#         print(f'Вы родились в {months[number_of_month]}. Птицы пели прекрасные песни.')
#     elif number_of_month in range(6, 9):
#         print(f'Вы родились в {months[number_of_month]}. Солнце светило ярче чем когда-либо.')
#     elif number_of_month in range(9, 12):
#         print(f'Вы родились в {months[number_of_month]}. Урожай был невероятным.')
#     else:
#         print(f'Вы родились в {months[number_of_month]}. За окном падал белый снег.')
# season_events(75)
