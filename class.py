# class Soda():
#     """определяет тип газировки"""
#
#     def __init__(self, dobavka):
#         self.dobavka = dobavka
#
#     def show_my_drink(self):
#         if isinstance(self.dobavka, str):
#             print('Газировка и ' + self.dobavka)
#         else:
#             print('Обычная газировка')
#
# water1 = Soda('персик')
# water2 = Soda(5)
#
# water1.show_my_drink()



# class TriangleChecker():
#     """проверяет возможность построить треугольник из заданных отрезков"""
#
#     def __init__(self, a, b, c):
#         self.a = a
#         self.b = b
#         self.c = c
#
#     def is_triangle(self):
#         if not isinstance(self.a, (int, float)) or not isinstance(self.b, (int, float)) or not isinstance(self.c, (int,float)):
#             print('Нужно вводить только числа!')
#         elif self.a < 0 or self.b < 0 or self.c < 0:
#             print('С отрицательными числами ничего не выйдет!')
#         elif self.a + self.b <= self.c or self.b + self.c <= self.a or self.c + self.a <= self.b:
#             print('Жаль, но из этого треугольник не сделать.')
#         else:
#             print('Ура, можно построить треугольник!')
#
# trg1 = TriangleChecker(5, 16.5, 16)
#
# trg1.is_triangle()
