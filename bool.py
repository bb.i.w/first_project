# def dislike_6(a):
#     if (type(a) is float or type(a) is int) and a == 6.0:
#         return 'Только не 6!'
#     return True
# print(dislike_6(6.50))


# def help_bool(letter=None):
#     if letter == 'к':
#         return 'A or B = B or A \nA and B = B and A'
#     if letter == 'а':
#         return 'A or (B or C) == (A or B) or C == A or B or C \n' \
#                'A and (B and C) == (A and B) and C == A and B and C'
#     if letter == 'д':
#         return 'A and (B or C) == (A and B) or (A and C) \nA or (B and C) == (A or B) and (A or C)'
#     if letter == 'м':
#         return 'not(A or B) == not A and not B \nnot(A and B) == not A or not B \n' \
#                'not(A < B) == A >= B \nnot(not(A)) = A'
#     else:
#         return 'Такого аргумента нет. Возможные:\n' \
#                'к – Коммутативность, \nд – Дистрибутивность, \nа – Ассоциативность, \nм – Теорема Де Моргана'
# print(help_bool('р'))


# def divider(a,b):
#     if b==0:
#         return 'Нули в знаменателе не приветствуются'
#     return (a/b)**3
# print(divider(2,6))

# def divinder(a,b):
#     return b and (a/b)**3 or 'Нули в знаменателе не привествуются'
# print(divinder(6,0))
