# def to_set(item):
#     new_set = set(item)
#     return new_set, len(new_set)
# print(to_set('1234567890'))


# from collections.abc import Hashable
# def list_to_set(lst):
#     st = {item for item in lst if isinstance(item, Hashable)}
#     return st
# print(list_to_set([1, 2, 3.14, {16,18,20}, '12345', '12345']))


# def diff(st1, st2, st3, symmetric=True):
#     if symmetric:
#         return st1^st2^st3
#     return st1-st2-st3
# st1={1,2,3,4,5}
# st2={3,4,5,6,7,8}
# st3={5,6,7,8,9,10,11}
# print(diff(st1, st2, st3))


# def superset(st1,st2):
#     if st1>st2:
#         print(f'Объект {st1} является чистым супермножеством')
#     elif st2>st1:
#         print(f'Объект {st2} является чистым супермножеством')
#     elif st1==st2:
#         print('Множества равны')
#     else:
#         print('Супермножество не обнаружено')
# st1={1,2,3,4,5}
# st2={1,2,3,4,5,6,7}
# superset(st1,st2)
